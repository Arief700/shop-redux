import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./Containers/Header";
import ProductDetail from "./Containers/ProductDetail";
import ProductListing from "./Containers/ProductListing";

const App = () => {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/" component={ProductListing} />
        <Route exact path="/product/:productId" component={ProductDetail} />
        <Route>404 Not Found!</Route>
      </Switch>
    </Router>
  );
};

export default App;
