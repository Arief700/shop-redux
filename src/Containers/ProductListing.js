import React,{useEffect} from "react";
import { useSelector,useDispatch } from "react-redux";
import { setProducts } from '../Redux/Actions/productsActions'
import axios from 'axios'
import ProductComponent from "./ProductComponent";

const ProductListing = () => {
  const product = useSelector((state) => state);
  const dispatch = useDispatch()

  const fetchProducts = async()=>{
    const response = await axios.get("https://fakestoreapi.com/products").catch((err)=>{
        console.log("Err",err);
    })

    dispatch(setProducts(response.data));
  }

  useEffect(() => {
      fetchProducts()
  }, [])
  console.log("product: ", product);
  return (
    <div className="ui grid container">
      <ProductComponent/>
    </div>
  );
};

export default ProductListing;
